package com.igor.ForMock;

public interface Algomeration {
    String getType(String village);
    String getName();
    String getNearRiver(String village);
    boolean isAirport(String village);
}
