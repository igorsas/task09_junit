package com.igor.ForMock;

import com.igor.ForMock.Algomeration;

public class Region {
    private Algomeration algomeration;
    private String name;
    public Region(String name, Algomeration algomeration){
        this.name = name;
        this.algomeration = algomeration;
    }

    public String getAlgomerationName(){ return algomeration.getName(); }
    public String getAlgomerationType(String village){ return algomeration.getType(village); }
    public String getNearestRiver(String village){return algomeration.getNearRiver(village); }
    public boolean checkAvailableAirport(String village){ return algomeration.isAirport(village); }

    public String getName(){ return name; }
    public boolean changeAlgomeration(Algomeration algomeration){
        this.algomeration = algomeration;
        return true;
    }
}
