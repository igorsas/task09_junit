package com.igor.JustTasks;

public class NotExperienceException extends RuntimeException {
    public NotExperienceException() {
        super();
    }

    public NotExperienceException(String message) {
        super(message);
    }

    public NotExperienceException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotExperienceException(Throwable cause) {
        super(cause);
    }

    protected NotExperienceException(String message, Throwable cause,
                        boolean enableSuppression,
                        boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
