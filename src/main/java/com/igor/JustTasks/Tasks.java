package com.igor.JustTasks;

public class Tasks {
    private String name;

    public Tasks(){
        name = "Epam";
    }

    public Tasks(String name){
        this.name = name;
    }
    public int getSalary(int experience) throws NotExperienceException {
        if(experience < 0){
            throw new NotExperienceException();
        }else{
            return experience*400;
        }
    }

    public boolean isNegativeNumber(int number){
        return number < 0;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
