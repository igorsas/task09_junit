package com.igor.JustTasks;

import org.junit.Rule;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.junit.rules.ExpectedException;

import static org.junit.jupiter.api.Assertions.*;

class TasksTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @DisplayName("Thrown exception test")
    @Test
    void getSalary() {
        Tasks tasks = new Tasks();
        assertThrows(NotExperienceException.class, () -> {
            tasks.getSalary(-2);
        });

    }

    @DisplayName("Incorrect number")
    @Test
    void isNegativeNumber() {
        Tasks tasks = new Tasks();
        assertFalse(tasks.isNegativeNumber(10));
    }

    @DisplayName("Equals name")
    @Test
    void setName() {
        Tasks tasks = new Tasks();
        tasks.setName("SoftServe");
        assertEquals(tasks.getName(), "SoftServe");
    }
}