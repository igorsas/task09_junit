package com.igor;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class ParameterizedTest {

    private int m1;
    private int m2;

    public ParameterizedTest(int p1, int p2) {
        m1 = p1;
        m2 = p2;
    }

    // creates the test data
    @Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][] { { 5 , 12 }, { 10, 0 }, { 144, 12 } };
        return Arrays.asList(data);
    }


    @Test
    public void testDivisionException() {
        MathOperations tester = new MathOperations();
        assertEquals("Result", m1 * m2, tester.divide(m1, m2));
    }

    @Test
    public void testSumListsException(){
        MathOperations tester = new MathOperations();
        List<Integer> list = new ArrayList<>(2);
        list.add(m1);
        list.add(m2);
        assertEquals("Result", list, tester.putInList(m1, m2));
    }


    // class to be tested
    class MathOperations {
        public int divide(int i, int j) {
            return i/j;
        }
        public List<Integer> putInList(int m1, int m2){
            List<Integer> list = new ArrayList<>(2);
            list.add(m1);
            list.add(m2);
            return list;
        }
    }


}