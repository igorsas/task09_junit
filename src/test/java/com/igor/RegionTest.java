package com.igor;

import com.igor.ForMock.Algomeration;
import com.igor.ForMock.Region;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RegionTest {

    @InjectMocks
    Region region;

    @Mock
    Algomeration algomeration;

    @Test
    void getAlgomerationType() {
        when(algomeration.getType("Parasha Without Roads")).thenReturn("Donbas");

        assertEquals(region.getAlgomerationType("Parasha Without Roads"), "Donbas");
    }

    @Test
    void getNearestRiver() {
        when(algomeration.getNearRiver("Muchosransk")).thenReturn("Gnila Lypa");

        assertEquals(region.getNearestRiver("Muchosransk"), "Gnila Lypa");
    }

    @Test
    void checkAvailableAirport() {
        when(algomeration.isAirport("Mukachevo")).thenReturn(false);

        assertFalse(region.checkAvailableAirport("Mukachevo"));
    }

    @Test
    void changeAlgomeration() {
        assertTrue(region.changeAlgomeration(algomeration));
    }
}